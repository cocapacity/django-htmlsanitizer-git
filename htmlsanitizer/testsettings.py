DATABASE_ENGINE = 'sqlite3'
DATABASE_NAME = './htmlsanitizer.db'
INSTALLED_APPS = ['htmlsanitizer']
ROOT_URLCONF = ['htmlsanitizer.urls']
